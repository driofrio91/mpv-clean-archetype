package com.mvp.driofrio.mvpcleanarchitecture.model;

import java.math.BigDecimal;

/**
 * Created by driofrio on 25/05/2015.
 */
public class Movie {

    private String tittle;
    private String descripcion;

    public Movie(String tittle, String descripcion) {
        this.tittle = tittle;
        this.descripcion = descripcion;
    }

    public Movie() {
    }


    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Movie{ tittle='" + tittle + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
