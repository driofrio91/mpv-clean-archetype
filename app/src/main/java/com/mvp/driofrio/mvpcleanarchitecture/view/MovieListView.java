package com.mvp.driofrio.mvpcleanarchitecture.view;

/**
 * Created by driofrio on 28/05/2015.
 */
public interface MovieListView {

    public void longClickItem(String mesanje);
    public void clickItem(String mensaje);
}
