package com.mvp.driofrio.mvpcleanarchitecture.interactor.impl;

import android.widget.Toast;

import com.mvp.driofrio.mvpcleanarchitecture.interactor.MovieUseCase;
import com.mvp.driofrio.mvpcleanarchitecture.interactor.executor.Executor;
import com.mvp.driofrio.mvpcleanarchitecture.interactor.executor.MainThread;
import com.mvp.driofrio.mvpcleanarchitecture.model.Movie;




import javax.security.auth.callback.Callback;

/**
 * Created by driofrio on 28/05/2015.
 */
public class MovieUseCaseImpl implements MovieUseCase {

    private Executor executor;
    private MainThread mainThread;

    private Movie movie;
    private Callback callback;

    public MovieUseCaseImpl(Executor executor, MainThread mainThread) {

        this.executor = executor;
        this.mainThread = mainThread;
    }

    @Override
    public void execute(Movie movie, Callback callback) {

        this.callback = callback;
        this.movie = movie;
        
        this.executor.run(this);

    }

    @Override
    public void run() {

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            callback.onError(e);
        }

        callback.onMissionAccomplished(this.movie);
    }
}
