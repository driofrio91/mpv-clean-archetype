package com.mvp.driofrio.mvpcleanarchitecture.presenter;

import com.mvp.driofrio.mvpcleanarchitecture.model.Movie;

/**
 * Created by driofrio on 25/05/2015.
 */
public interface MovieListPresenter extends Presenter{

    public void onClickItem(Movie movie);
    public void onLongClickItem(Movie movie);

}
