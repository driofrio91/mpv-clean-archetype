package com.mvp.driofrio.mvpcleanarchitecture.presenter;

/**
 * Created by driofrio on 27/05/2015.
 */
public interface Presenter {

    public void onInit();
}
