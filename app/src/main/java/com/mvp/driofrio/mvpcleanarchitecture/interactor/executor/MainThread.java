package com.mvp.driofrio.mvpcleanarchitecture.interactor.executor;

/**
 * Created by toni on 03/02/15.
 */
public interface MainThread {
    void post(final Runnable runnable);
}
