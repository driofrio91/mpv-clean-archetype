package com.mvp.driofrio.mvpcleanarchitecture.presenter.impl;

import com.mvp.driofrio.mvpcleanarchitecture.interactor.MovieUseCase;
import com.mvp.driofrio.mvpcleanarchitecture.model.Movie;
import com.mvp.driofrio.mvpcleanarchitecture.presenter.MovieListPresenter;
import com.mvp.driofrio.mvpcleanarchitecture.view.MovieListView;

/**
 * Created by driofrio on 27/05/2015.
 */
public class MovieListPresenterImpl implements MovieListPresenter {



    private MovieUseCase movieUseCase;
    private MovieListView movieListView;

    public MovieListPresenterImpl(MovieUseCase movieUseCase) {
        this.movieUseCase = movieUseCase;
    }

    @Override
    public void onInit() {

    }

    @Override
    public void onClickItem(Movie movie) {
        movieUseCase.execute(movie, new MovieUseCase.Callback() {
            @Override
            public void onMissionAccomplished(Movie movie) {
                movieListView.clickItem(movie.toString());
            }

            @Override
            public void onError(Exception ex) {
                movieListView.clickItem(ex.getMessage());
            }
        });
    }

    @Override
    public void onLongClickItem(Movie movie) {
        movieUseCase.execute(movie, new MovieUseCase.Callback() {
            @Override
            public void onMissionAccomplished(Movie movie) {
                movieListView.clickItem(movie.toString());
            }

            @Override
            public void onError(Exception ex) {
                movieListView.clickItem(ex.getMessage());
            }
        });
    }
}
