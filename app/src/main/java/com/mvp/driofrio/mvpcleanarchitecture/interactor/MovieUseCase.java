package com.mvp.driofrio.mvpcleanarchitecture.interactor;

import com.mvp.driofrio.mvpcleanarchitecture.interactor.executor.Interactor;
import com.mvp.driofrio.mvpcleanarchitecture.model.Movie;

import javax.security.auth.callback.Callback;

/**
 * Created by driofrio on 27/05/2015.
 */
public interface MovieUseCase extends Interactor{

    interface Callback {
        void onMissionAccomplished(Movie movie);
        void onError(Exception ex);
    }

    public void execute(Movie movie, final Callback callback);
}
